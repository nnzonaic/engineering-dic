import 'package:drift/drift.dart';
import 'package:engineering_dictionary/database/dictionary_database.dart';
import 'package:flutter/material.dart';

class DetailScreen extends StatefulWidget {
  final DictionaryTableData data;
  final DictionaryDatabase database;
  DetailScreen({required this.data, required this.database, key})
      : super(key: key);

  @override
  State<DetailScreen> createState() => _DetailScreenState();
}

class _DetailScreenState extends State<DetailScreen> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        actions: [
          StreamBuilder<DictionaryTableData>(
            stream: widget.database.dictionaryDao.watchDetail(widget.data.id),
            builder: (context, snapshot) {
              if (snapshot.hasData) {
                return IconButton(
                  onPressed: () async {
                    if (snapshot.data?.favourite == null) {
                      await widget.database.dictionaryDao.updateWords(
                        DictionaryTableCompanion(
                          id: Value(widget.data.id),
                          eng: Value(widget.data.eng),
                          type: Value(widget.data.type),
                          myan: Value(widget.data.myan),
                          favourite: const Value(true),
                        ),
                      );
                    } else {
                      await widget.database.dictionaryDao.updateWords(
                        DictionaryTableCompanion(
                          id: Value(widget.data.id),
                          eng: Value(widget.data.eng),
                          type: Value(widget.data.type),
                          myan: Value(widget.data.myan),
                          favourite: const Value(null),
                        ),
                      );
                    }
                    setState(() {});
                    print(widget.data.favourite);
                    if (snapshot.data?.favourite == null) {
                      // ignore: use_build_context_synchronously
                      ScaffoldMessenger.of(context).showSnackBar(SnackBar(
                          backgroundColor:
                              const Color.fromRGBO(143, 148, 251, 1),
                          duration: const Duration(milliseconds: 500),
                          content: Text(
                              '"${widget.data.eng}" Added to Favourite List')));
                    } else if (snapshot.data?.favourite == true) {
                      // ignore: use_build_context_synchronously
                      ScaffoldMessenger.of(context).showSnackBar(
                        SnackBar(
                          backgroundColor:
                              const Color.fromRGBO(143, 148, 251, 1),
                          duration: const Duration(milliseconds: 500),
                          content: Text(
                              '"${widget.data.eng}" Removed from Favourite List'),
                        ),
                      );
                    }
                  },
                  icon: snapshot.data?.favourite == null ||
                          snapshot.data?.favourite == false
                      ? const Icon(Icons.favorite_outline)
                      : const Icon(
                          Icons.favorite,
                          color: Color.fromARGB(255, 230, 30, 63),
                        ),
                );
              } else if (snapshot.hasError) {
                return const Icon(Icons.error);
              }
              return const Center(child: CircularProgressIndicator());
            },
          ),
        ],
        backgroundColor: const Color.fromARGB(255, 111, 115, 201),
        title: Text(widget.data.eng),
        centerTitle: true,
      ),
      body: ListView(children: [
        Padding(
          padding: const EdgeInsets.all(20.0),
          child: ListTile(
            title: const Padding(
              padding: EdgeInsets.only(bottom: 8),
              child: Text(
                'English',
                style: TextStyle(
                  fontSize: 20,
                  fontWeight: FontWeight.bold,
                ),
              ),
            ),
            subtitle: Text(
              widget.data.eng,
              style: const TextStyle(
                fontSize: 17,
              ),
            ),
          ),
        ),
        const Divider(
          color: Color.fromRGBO(143, 148, 251, 1),
        ),
        Padding(
          padding: const EdgeInsets.all(20.0),
          child: ListTile(
            title: const Padding(
              padding: EdgeInsets.only(bottom: 8.0),
              child: Text(
                'Type',
                style: TextStyle(
                  fontSize: 20,
                  fontWeight: FontWeight.bold,
                ),
              ),
            ),
            subtitle: Text(
              widget.data.type,
              style: const TextStyle(
                fontSize: 17,
              ),
            ),
          ),
        ),
        const Divider(
          color: Color.fromRGBO(143, 148, 251, 1),
        ),
        Padding(
          padding: const EdgeInsets.all(20.0),
          child: ListTile(
            title: const Padding(
              padding: EdgeInsets.only(bottom: 8.0),
              child: Text(
                'Myanmar',
                style: TextStyle(
                  fontSize: 20,
                  fontWeight: FontWeight.bold,
                ),
              ),
            ),
            subtitle: Text(
              widget.data.myan,
              style: const TextStyle(
                fontSize: 17,
              ),
            ),
          ),
        ),
      ]),
    );
  }
}
