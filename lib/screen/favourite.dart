import 'package:engineering_dictionary/database/dictionary_database.dart';
import 'package:flutter/material.dart';
import 'package:flutter/src/foundation/key.dart';
import 'package:flutter/src/widgets/framework.dart';

import 'detail.dart';

class FavouriteList extends StatelessWidget {
  final DictionaryDatabase dictionaryDatabase;
  FavouriteList({required this.dictionaryDatabase, key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        backgroundColor: const Color.fromARGB(255, 111, 115, 201),
        title: const Text('Favourite'),
        centerTitle: true,
      ),
      body: Column(children: [
        Expanded(
          child: Padding(
            padding: const EdgeInsets.only(
              left: 20,
              right: 20,
              top: 10,
            ),
            child: FutureBuilder<List<DictionaryTableData>>(
              future: dictionaryDatabase.dictionaryDao.favouriteList(),
              builder: (context, snapshot) {
                if (snapshot.hasData) {
                  return ListView.builder(
                    itemCount: snapshot.data!.length,
                    itemBuilder: (context, index) {
                      return InkWell(
                        onTap: () {
                          Navigator.push(context,
                              MaterialPageRoute(builder: (_) {
                            return DetailScreen(
                              data: snapshot.data![index],
                              database: dictionaryDatabase,
                            );
                          }));
                        },
                        child: Card(
                          elevation: 0,
                          margin: const EdgeInsets.symmetric(vertical: 2),
                          child: ListTile(
                            title: Text(snapshot.data?[index].eng ?? ''),
                            subtitle: Text(snapshot.data?[index].type ?? ''),
                            trailing: const Icon(
                              Icons.favorite,
                              color: Color.fromARGB(255, 230, 30, 63),
                            ),
                          ),
                        ),
                      );
                    },
                  );
                } else if (snapshot.hasError) {
                  return const Text('error');
                }
                return const CircularProgressIndicator();
              },
            ),
          ),
        )
      ]),
    );
  }
}
