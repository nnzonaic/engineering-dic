import 'package:engineering_dictionary/database/dictionary_database.dart';
import 'package:engineering_dictionary/screen/detail.dart';
import 'package:engineering_dictionary/screen/favourite.dart';
import 'package:flutter/material.dart';

class Home extends StatefulWidget {
  const Home({Key? key}) : super(key: key);

  @override
  State<Home> createState() => _HomeState();
}

class _HomeState extends State<Home> {
  DictionaryDatabase dictionaryDatabase = DictionaryDatabase();

  TextEditingController _textEditingController = TextEditingController();
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: const Text('Engineering Dictionary'),
        centerTitle: true,
        backgroundColor: const Color.fromARGB(255, 111, 115, 201),
      ),
      floatingActionButton: Padding(
        padding: const EdgeInsets.only(bottom: 30),
        child: FloatingActionButton(
          backgroundColor: const Color.fromRGBO(143, 148, 251, 1),
          onPressed: () {
            Navigator.push(context, MaterialPageRoute(builder: (context) {
              return FavouriteList(dictionaryDatabase: dictionaryDatabase);
            }));
          },
          child: const Icon(Icons.favorite),
        ),
      ),
      body: Column(
        children: [
          Card(
            margin: const EdgeInsets.all(20),
            shape: RoundedRectangleBorder(
              borderRadius: BorderRadius.circular(10),
            ),
            elevation: 5,
            child: Padding(
              padding: const EdgeInsets.only(left: 15, right: 15),
              child: TextField(
                cursorColor: const Color.fromRGBO(143, 148, 251, 1),
                controller: _textEditingController,
                onChanged: (value) {
                  setState(() {});
                },
                decoration: const InputDecoration(
                  border: InputBorder.none,
                  hintText: ' Search',
                  hintStyle: TextStyle(color: Color.fromRGBO(143, 148, 251, 1)),
                  suffixIcon: Icon(
                    Icons.search,
                    color: Color.fromRGBO(143, 148, 251, 1),
                  ),
                ),
              ),
            ),
          ),
          Expanded(
            child: Padding(
              padding: const EdgeInsets.only(
                left: 20,
                right: 20,
                top: 10,
              ),
              child: FutureBuilder<List<DictionaryTableData>>(
                future: dictionaryDatabase.dictionaryDao
                    .searchWord(_textEditingController.text),
                builder: (context, snapshot) {
                  if (snapshot.hasData) {
                    return ListView.builder(
                      itemCount: snapshot.data!.length,
                      itemBuilder: (context, index) {
                        return InkWell(
                          onTap: () {
                            Navigator.push(context,
                                MaterialPageRoute(builder: (_) {
                              return DetailScreen(
                                data: snapshot.data![index],
                                database: dictionaryDatabase,
                              );
                            }));
                          },
                          child: Card(
                            elevation: 0,
                            margin: const EdgeInsets.symmetric(vertical: 2),
                            child: ListTile(
                              title: Text(snapshot.data?[index].eng ?? ''),
                              subtitle: Text(snapshot.data?[index].type ?? ''),
                            ),
                          ),
                        );
                      },
                    );
                  } else if (snapshot.hasError) {
                    return const Text('error');
                  }
                  return const CircularProgressIndicator();
                },
              ),
            ),
          )
        ],
      ),
    );
  }
}
