import 'package:drift/drift.dart';
import 'package:engineering_dictionary/database/dictionary_database.dart';
import 'package:engineering_dictionary/database/dictionary_table.dart';

part 'dictionary_dao.g.dart';

@DriftAccessor(tables: [DictionaryTable])
class DictionaryDao extends DatabaseAccessor<DictionaryDatabase>
    with _$DictionaryDaoMixin {
  DictionaryDao(DictionaryDatabase dictionaryDatabase)
      : super(dictionaryDatabase);

  Future<List<DictionaryTableData>> getAllWorld() async {
    return await select(dictionaryTable).get();
  }

  Future<List<DictionaryTableData>> searchWord(String words) async {
    return await (select(dictionaryTable)
          ..where((tbl) => tbl.eng.like('$words%')))
        .get();
  }

  Future<bool> updateWords(DictionaryTableCompanion data) async {
    return await update(dictionaryTable).replace(data);
  }

  Future<List<DictionaryTableData>> favouriteWords() async {
    return await (select(dictionaryTable)
          ..where((tbl) => tbl.favourite.isNotNull()))
        .get();
  }

  Stream<DictionaryTableData> watchDetail(int id) { 
    return (select(dictionaryTable)..where((tbl) => tbl.id.equals(id)))
        .watchSingle();
  }

  Future<List<DictionaryTableData>> favouriteList() async {
    return await (select(dictionaryTable)
          ..where((tbl) => tbl.favourite.equals(true)))
        .get();
  }
}
